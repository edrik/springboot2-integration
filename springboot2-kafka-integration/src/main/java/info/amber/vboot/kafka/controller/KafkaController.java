package info.amber.vboot.kafka.controller;

import info.amber.vboot.kafka.service.KafkaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/kafka")
@Slf4j
public class KafkaController {

    @Autowired
    private KafkaService kafkaService;

    @GetMapping
    public Object get() {
        log.info("send msg start ...");
        kafkaService.send(KafkaService.TOPIC_TEST, "balabala...");
        kafkaService.send(KafkaService.TOPIC_TEST, "hahahaha..");
        log.info("send msg end");
        return "ok";
    }

}
