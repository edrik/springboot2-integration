package info.amber.jdk8.test;

public class TestJ8FunctionalInterface2 {

	public static void main(String[] args) {
		TestJ8FunctionalInterface2 interface2 = new TestJ8FunctionalInterface2();
		interface2.test(10, () -> System.out.println("a customed Func."));

		interface2.test(100, interface2::customerdFunc);
	}

	public void customerdFunc() {
		System.out.println("a customed method reference.");
	}

	public void test(int x, Func func) {
		System.out.println(x);
		func.run();
	}


}
