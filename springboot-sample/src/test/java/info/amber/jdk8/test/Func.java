package info.amber.jdk8.test;

@FunctionalInterface
public interface Func extends NonFunc {

	void run();

	default void foo() {

	}

	default void voo() {

	}
}
