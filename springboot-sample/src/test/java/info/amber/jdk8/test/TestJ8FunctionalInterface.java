package info.amber.jdk8.test;


public class TestJ8FunctionalInterface implements Func {

	@Override
	public void run() {
		System.out.println("runc");
	}

	@Override
	public void foo() {

		System.out.println("foo");
	}

	@Override
	public void voo() {
		System.out.println("voo");
	}

	public static void main(String[] args) {
		Func func = new TestJ8FunctionalInterface();
		func.run();
		func.foo();
		func.voo();
	}

}
