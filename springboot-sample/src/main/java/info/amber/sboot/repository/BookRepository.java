package info.amber.sboot.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import info.amber.sboot.domain.Book;

@Repository
public interface BookRepository extends PagingAndSortingRepository<Book, Long> {
	Book findBookByIsbn(String isbn);

	void deleteBookByIsbn(String isbn);
}

