package info.amber.sboot.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import info.amber.sboot.domain.Author;

@Repository
public interface AuthorRepository extends CrudRepository<Author, Long> {

}

