package info.amber.sboot.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import info.amber.sboot.domain.Publisher;

@Repository
public interface PublisherRepository extends CrudRepository<Publisher, Long> {
}
