package info.amber.sboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import info.amber.sboot.domain.User;

//@RepositoryRestResource(path = "user", excerptProjection = ListUser.class)
@RepositoryRestResource(path = "user")
public interface UserJpaRepository extends JpaRepository<User, Long> {

	@RestResource(exported = false)
	@Override
	public void delete(Long id);

	//
	@RestResource(path = "name", rel = "name")
	public User findByName(@Param("name") String name);

	//
	// @RestResource(path = "nameAndAge", rel = "nameAndAge")
	// public List<User> findByNameAndAge(@Param("name") String name, @Param("age") int age);
	//
	// @RestResource(path = "nameStartsWith", rel = "nameStartsWith")
	// public List<User> findByNameStartsWith(@Param("name") String name);
	//
	// @RestResource(path = "nameStartsWithIgnore", rel = "nameStartsWithIgnore")
	// public List<User> findByNameStartsWithIgnoringCase(@Param("name") String name);
	//
	// @RestResource(path = "nameStartsWithDesc", rel = "nameStartsWithDesc")
	// public List<User> findByNameStartsWithOrderByAgeDesc(@Param("name") String name);

}