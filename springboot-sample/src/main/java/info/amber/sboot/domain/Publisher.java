package info.amber.sboot.domain;

import javax.persistence.*;
import java.util.List;

/**
 * 作者：杜琪 链接：http:// www.jianshu.com/p/1b626a6f550e
 * 來源：简书 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 *
 */
@Entity(name = "boot_publisher")
public class Publisher {
	@Id
	@GeneratedValue(generator = "BookPublisherId", strategy = GenerationType.TABLE)
	@TableGenerator(name = "BookPublisherId", table = "SEQUENCE_GENERATOR", pkColumnName = "ID_NAME", valueColumnName = "ID_VAL", pkColumnValue = "BookPublisherId",
			allocationSize = 1)
	@Column(name = "id", unique = true, nullable = false)
	private Long	id;
	private String	name;

	@OneToMany(mappedBy = "publisher")
	private List<Book> books;

	protected Publisher() {
	}

	public Publisher(String name) {
		this.name = name;
	}

	@Entity(name = "boot_reviewer")
	public class Reviewer {
		@Id
		@GeneratedValue
		private Long	id;
		private String	firstName;
		private String	lastName;

		protected Reviewer() {
		}

		public Reviewer(String firstName, String lastName) {
			this.firstName = firstName;
			this.lastName = lastName;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

}
