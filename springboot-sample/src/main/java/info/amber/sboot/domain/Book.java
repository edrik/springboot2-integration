package info.amber.sboot.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.TableGenerator;

@Entity(name = "boot_books")
public class Book {
	@Id
	@GeneratedValue(generator = "BookId", strategy = GenerationType.TABLE)
	@TableGenerator(name = "BookId", table = "SEQUENCE_GENERATOR", pkColumnName = "ID_NAME", valueColumnName = "ID_VAL", pkColumnValue = "BookId", allocationSize = 1)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	private String isbn;

	private String title;

	private String description;

	@ManyToOne
	private Author		author;
	@ManyToOne
	private Publisher	publisher;

	@ManyToMany
	private List<Publisher.Reviewer> reviewers;

	protected Book() {
	}

	public Book(Author author, String isbn, Publisher publisher, String title) {
		this.author = author;
		this.isbn = isbn;
		this.publisher = publisher;
		this.title = title;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	public List<Publisher.Reviewer> getReviewers() {
		return reviewers;
	}

	public void setReviewers(List<Publisher.Reviewer> reviewers) {
		this.reviewers = reviewers;
	}

}
