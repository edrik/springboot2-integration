package info.amber.sboot;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.jdbc.core.JdbcTemplate;

import info.amber.sboot.repository.BookRepository;

/**
 * 作者：
 * 
 * 杜琪 链接：http:// www.jianshu.com/p/1b626a6f550e
 * 來源：
 * 简书 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 *
 */
public class StartupRunner implements CommandLineRunner {
	protected static final Logger logger = LoggerFactory.getLogger(StartupRunner.class);

	@Autowired
	private DataSource ds;

	@Autowired
	private BookRepository bookRepository;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public void run(String... strings) throws Exception {
		logger.info("Datasource: " + ds.toString());

		logger.info("Number of books: " + bookRepository.count());
	}
}
