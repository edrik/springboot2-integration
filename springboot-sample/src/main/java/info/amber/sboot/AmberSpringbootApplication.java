package info.amber.sboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;

//@EnableAutoConfiguration
@SpringBootApplication
@EnableCaching
public class AmberSpringbootApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		// 注意这里要指向原先用main方法执行的Application启动类
		return application.sources(AmberSpringbootApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(AmberSpringbootApplication.class, args);
	}
	/**
	 * start your application as a war or as an executable application
	 * https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#howto-create-a-deployable-war-file
	 */
	// private static SpringApplicationBuilder configureApplication(SpringApplicationBuilder builder) {
	// return builder.sources(AmberSpringbootApplication.class).bannerMode(Banner.Mode.OFF);
	// }
}