package info.amber.sboot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.google.common.base.Charsets;
import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnel;
import com.google.common.hash.PrimitiveSink;

public class BloomFilterHelper {
	private static final Logger logger = LoggerFactory.getLogger(BloomFilterHelper.class);

	//
	private final BloomFilter<String> bloomFliter = BloomFilter.create(new Funnel<String>() {

		private static final long serialVersionUID = 1L;

		@Override
		public void funnel(String from, PrimitiveSink into) {
			into.putString(from, Charsets.UTF_8);
		}
	}, 1024 * 1024 * 32, 0.0000001d);

	public synchronized void addToFloomFliter(String obj) {
		bloomFliter.put(obj);
	}

	public synchronized boolean containsDealId(String str) {

		if (StringUtils.isEmpty(str)) {
			logger.warn("promoCode is null");
			return true;
		}

		boolean exists = bloomFliter.mightContain(str);
		if (!exists) {
			bloomFliter.put(str);
		}
		return exists;
	}
}
