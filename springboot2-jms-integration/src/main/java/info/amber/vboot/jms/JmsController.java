package info.amber.vboot.jms;

import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/jms")
public class JmsController {

    @Autowired private JmsService jmsService;


    @GetMapping
    public  Object get(){
        Map res = Maps.newHashMap();

        res.put("code","ok");
        res.put("data","hello jms activeMQ");
        jmsService.send(res);
        return res;
    }
}
