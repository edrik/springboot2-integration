package info.amber.vboot.jms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class Springboot2JmsIntegrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springboot2JmsIntegrationApplication.class, args);
    }



}
