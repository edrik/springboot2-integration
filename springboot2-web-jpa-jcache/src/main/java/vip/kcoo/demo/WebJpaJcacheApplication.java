package vip.kcoo.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableCaching
@SpringBootApplication
public class WebJpaJcacheApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebJpaJcacheApplication.class, args);
    }

    @GetMapping("/demo")
    public Object get() {
        return "aliya";
    }
}
