package me.info.sboot.func.test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import me.info.common.func.ThrowingConsumer;

/**
 * @author Amber L
 * @version v0.0.1
 * @desc
 * @date 2020/12/26 11:44
 */
public class TrysTest {

  public static void main(String[] args) {
    test_throwable();
  }


  public static void test_throwable() {
    List<Integer> integers = Arrays.asList(2, 3, 4, 5, 6, 7, 8, 9, 12);
    // integers.forEach(consumerWrapper(i -> System.out.println(333 / i),
    // ArithmeticException.class));
    integers.forEach(throwingConsumerWrapper(i -> System.out.println(333 / i)));
  }

  static Consumer<Integer> lambdaWrapper(Consumer<Integer> consumer) {
    return i -> {
      try {
        consumer.accept(i);
      } catch (ArithmeticException e) {
        e.printStackTrace();
      }
    };
  }

  static <T, E extends Exception> Consumer<T> consumerWrapper(
      Consumer<T> consumer, Class<E> clazz) {

    return i -> {
      try {
        consumer.accept(i);
      } catch (Exception ex) {
        try {
          E exCast = clazz.cast(ex);
          System.err.println("Exception occured : " + exCast.getMessage());
        } catch (ClassCastException ccEx) {
          throw ex;
        }
      }
    };
  }

  static <T> Consumer<T> throwingConsumerWrapper(ThrowingConsumer<T, Exception> throwingConsumer) {

    return i -> {
      try {
        throwingConsumer.accept(i);
      } catch (Exception ex) {
        throw new RuntimeException(ex);
      }
    };
  }

  static <T, E extends Exception> Consumer<T> handlingConsumerWrapper(
      ThrowingConsumer<T, E> throwingConsumer, Class<E> exceptionClass) {

    return i -> {
      try {
        throwingConsumer.accept(i);
      } catch (Exception ex) {
        try {
          E exCast = exceptionClass.cast(ex);
          System.err.println("Exception occured : " + exCast.getMessage());
        } catch (ClassCastException ccEx) {
          throw new RuntimeException(ex);
        }
      }
    };
  }
}
