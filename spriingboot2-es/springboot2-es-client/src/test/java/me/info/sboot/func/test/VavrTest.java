package me.info.sboot.func.test;

import org.junit.jupiter.api.Test;

import io.vavr.control.Try;

/**
 * @author Amber L
 * @version v0.0.1
 * @desc
 * @date 2020/12/26 12:29
 */
public class VavrTest {

  @Test
  public void vavr_try_test() {
    Try<Integer> res = Try.of(() -> 1 / 0);
    int errorSentinel = res.getOrElse(-1);
    assert res.isFailure();
  }



}
