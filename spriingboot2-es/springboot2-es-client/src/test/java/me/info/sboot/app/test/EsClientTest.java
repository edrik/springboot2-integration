package me.info.sboot.app.test;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.common.xcontent.json.JsonXContent;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import lombok.extern.slf4j.Slf4j;
import me.info.common.Jsons;
import me.info.sboot.app.domain.News;

/**
 * @author Amber
 * @version v0.0.1
 * @desc
 * @date 2020/12/21 15:49
 */
@Slf4j
@SpringBootTest
public class EsClientTest {

  private String index;
  private String type;

  @Qualifier("rhlClient")
  @Autowired
  private RestHighLevelClient rhlClient;

  @Qualifier("restClient")
  @Autowired
  private RestClient client;

  @BeforeEach
  public void prepare() {
    index = "demo";
    type = "demo";
  }

  @Test
  public void batchAddTest() {
    BulkRequest bulkRequest = new BulkRequest();
    List<IndexRequest> requests = genRequests();
    for (IndexRequest request : requests) {
      bulkRequest.add(request);
    }
    try {
      rhlClient.bulk(bulkRequest, RequestOptions.DEFAULT);
    } catch (IOException e) {
      log.error(e.getMessage(), e);
    }
  }

  public List<IndexRequest> genRequests() {
    List<IndexRequest> requests = new ArrayList<>();
    requests.add(generateNewsRequest("中印边防军于拉达克举行会晤 强调维护边境和平", "军事", "2018-01-27T08:34:00Z"));
    requests.add(generateNewsRequest("费德勒收郑泫退赛礼 进决赛战西里奇", "体育", "2018-01-26T14:34:00Z"));
    requests.add(generateNewsRequest("欧文否认拿动手术威胁骑士 兴奋全明星联手詹皇", "体育", "2018-01-26T08:34:00Z"));
    requests.add(generateNewsRequest("皇马官方通告拉莫斯伊斯科伤情 将缺阵西甲关键战", "体育", "2018-01-26T20:34:00Z"));
    return requests;
  }

  public IndexRequest generateNewsRequest(String title, String tag, String publishTime) {
    IndexRequest indexRequest = new IndexRequest(index);
    News news = new News();
    news.setTitle(title);
    news.setTag(tag);
    news.setPublishTime(LocalDateTime.now());
    String source = Jsons.json(news);
    indexRequest.source(source, XContentType.JSON);

    return indexRequest;
  }

  @Disabled
  @Test
  public void indexTest() {
    try {
      // 借助indexRequest的json拼接工具
      CreateIndexRequest indexRequest = new CreateIndexRequest("demo");
      XContentBuilder builder =
          JsonXContent.contentBuilder()
              .startObject()
              //.startObject("mappings")
              .startObject("properties")
              .startObject("title")
              .field("type", "text")
              .field("analyzer", "ik_max_word")
              .endObject()
              .startObject("content")
              .field("type", "text")
              .field("index", "true")
              .field("analyzer", "ik_max_word")
              .endObject()
              .startObject("uniqueId")
              .field("type", "keyword")
              .field("index", "false")
              .endObject()
              .startObject("created")
              .field("type", "date")
              .field("format", "strict_date_optional_time||epoch_millis")
            //  .endObject()
              .endObject()
              .endObject()
              .endObject();
      indexRequest.mapping(builder);
      System.out.println( Strings.toString(builder));
      XContentBuilder settingBuilder =JsonXContent.contentBuilder()
                                                  .startObject()
                                                  //.startObject("settings")
                                                  .startObject("index")
                                                  .field("number_of_shards", 3)
                                                  .field("number_of_replicas", 2)
                                                  .endObject()
                                                  //.endObject()
                                                  .endObject();
      String settings = Strings.toString( settingBuilder);
      System.out.println(settings);
      indexRequest.settings(settingBuilder)
         ;
      // 生成json字符串
      /*String settings = indexRequest.settings().toString();
      System.out.println(settings);
      String source = indexRequest.source().utf8ToString();
      HttpEntity entity = new NStringEntity(source, ContentType.APPLICATION_JSON);
      Response response =
          client.performRequest("put", "/demo", Collections.<String, String>emptyMap(), entity);
      System.out.println(response);*/
      // 使用RestClient进行操作 而非rhlClient
      rhlClient.indices().create(indexRequest, RequestOptions.DEFAULT);
      /**/
    } catch (IOException e1) {
      e1.printStackTrace();
    }
  }

  @Test
  public void testQuery() {
    SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
    sourceBuilder.from(0);
    sourceBuilder.size(10);
    sourceBuilder.fetchSource(new String[] {"title"}, new String[] {});
    MatchQueryBuilder matchQueryBuilder = QueryBuilders.matchQuery("title", "费德勒");
    TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("tag", "体育");
    RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("publishTime");
    rangeQueryBuilder.gte("2018-01-26T08:00:00Z");
    rangeQueryBuilder.lte("2018-01-26T20:00:00Z");
    BoolQueryBuilder boolBuilder = QueryBuilders.boolQuery();
    boolBuilder.must(matchQueryBuilder);
    boolBuilder.must(termQueryBuilder);
    boolBuilder.must(rangeQueryBuilder);
    sourceBuilder.query(boolBuilder);
    SearchRequest searchRequest = new SearchRequest(index);
    searchRequest.types(type);
    searchRequest.source(sourceBuilder);
    try {
      SearchResponse response = rhlClient.search(searchRequest, RequestOptions.DEFAULT);
      System.out.println(response);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  @Test
  public void updateTest() {
    String id = "bsVChHYBDlXluyaRRDLM";

    UpdateRequest updateRequest = new UpdateRequest(index /*, type*/, id);

    Map<String, Object> map = new HashMap<>();
    map.put("tag", "网球");

    updateRequest.doc(map);
    try {
      rhlClient.update(updateRequest, RequestOptions.DEFAULT);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
