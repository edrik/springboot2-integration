package me.info.sboot.app.test;

import java.io.IOException;
import java.util.Arrays;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import lombok.extern.slf4j.Slf4j;
import me.info.common.Jsons;
import me.info.sboot.app.holder.EsTemplate;

/**
 * @author Amber
 * @version v0.0.1
 * @desc
 * @date 2020/12/21 10:54
 */
@Slf4j
@SpringBootTest
public class EsSearchSampleTest {

  @Autowired EsTemplate esTemplate;
  @Autowired RestHighLevelClient rhlClient;

  @Test
  public void testSearchAll() throws IOException {
    // 基础设置
    SearchRequest searchRequest = new SearchRequest("demo");
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    // 搜索方式
    searchSourceBuilder.query(QueryBuilders.matchAllQuery());
    searchRequest.source(searchSourceBuilder);

    // 返回字段过滤（可选）
    // 第一个参数为结果集包含哪些字段，第二个参数为结果集不包含哪些字段
    // searchSourceBuilder.fetchSource(new String[]{"name","description"}, new String[]{});

    // 发起请求，获取结果
    SearchResponse searchResponse = rhlClient.search(searchRequest, RequestOptions.DEFAULT);

    // 打印结果集
    printResult(searchResponse);
  }

  @Test
  public void testPage_Bool_MultiMatchSearch() throws IOException {
    // 定义一个布尔query，将两个条件组合在一起
    BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
    boolQueryBuilder.must(
        QueryBuilders.multiMatchQuery("Spring微服务实战", "name", "description")
            .minimumShouldMatch("70%")
            .field("name", 10));
    boolQueryBuilder.must(QueryBuilders.termQuery("price", 86.5));
    // 过滤器
    boolQueryBuilder.filter(QueryBuilders.termQuery("price", 86.6)); // ?
    // 一定要转成小写 toLowerCase() 否则搜索不到，加上以后大小写都可搜索到
    // 注意 searchSourceBuilder 默认返回10 条数据，如果需要返回实级条数需要设置
    BoolQueryBuilder queryBuilder =
        QueryBuilders.boolQuery()
            .should(QueryBuilders.wildcardQuery("case_number", ("*201910*").toLowerCase()));

    // 发起请求，获取结果
    SearchResponse resp = esTemplate.searchPage(queryBuilder, 90L, 1, 10, "caselist");
    printResult(resp);
  }

  @Test
  public void test_filter() throws IOException {
    // 基础设置
    SearchRequest searchRequest = new SearchRequest("bigsea_demo");
    // 搜索源构建对象
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    // bool搜索
    // 第一个query
    MultiMatchQueryBuilder multiMatchQueryBuilder =
        QueryBuilders.multiMatchQuery("Spring微服务实战", "name", "description")
            .minimumShouldMatch("70%")
            .field("name", 10);
    // 第二个query
    TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("price", 86.5);
    // 定义一个布尔query，将上面两个条件组合在一起
    BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
    boolQueryBuilder.must(multiMatchQueryBuilder);
    boolQueryBuilder.must(termQueryBuilder);
    // 过滤器
    boolQueryBuilder.filter(QueryBuilders.termQuery("price", 86.6));
    // 设置
    searchSourceBuilder.query(boolQueryBuilder);
    // 定义排序
    searchSourceBuilder.sort(new FieldSortBuilder("price").order(SortOrder.ASC));

    // 给搜索请求对象设置搜索源
    searchRequest.source(searchSourceBuilder);
    // 发起请求，获取结果
    SearchResponse searchResponse = rhlClient.search(searchRequest, RequestOptions.DEFAULT);
    printResult(searchResponse);
  }

  @Test
  public void testMatchSearch() {
    try {
      // QueryBuilders.matchQuery("description", "spring实战").operator(Operator.OR)
      QueryBuilder queryBuilder =
          QueryBuilders.matchQuery("relationShip", "one integrated can not replace")
              .minimumShouldMatch("1%");
      SearchResponse resp = esTemplate.search(queryBuilder, 660, "product");
      printResult(resp);

    } catch (IOException e) {
      log.error(e.getMessage(), e);
    }
  }

  /**
   * 根据关键字搜索（多个域）
   *
   * @throws IOException
   */
  @Test
  public void testMultiMatchSearch() throws IOException {
    // 基础设置
    SearchRequest searchRequest = new SearchRequest("bigsea_demo");
    // 搜索源构建对象
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    // matchQuery（以name、descrption两个域为搜索域，并且至少匹配到70%的词）
    MultiMatchQueryBuilder multiMatchQueryBuilder =
        QueryBuilders.multiMatchQuery("Spring微服务实战", "name", "description")
            .minimumShouldMatch("70%")
            .field("name", 10);
    searchSourceBuilder.query(multiMatchQueryBuilder);
    // 给搜索请求对象设置搜索源
    searchRequest.source(searchSourceBuilder);
    // 发起请求，获取结果
    SearchResponse searchResponse = rhlClient.search(searchRequest, RequestOptions.DEFAULT);
    // 打印结果集
    printResult(searchResponse);
  }

  @Test
  public void testSearch() throws IOException {
    // term query查询 该查询为精确查询（不会对查询条件进行分词），在查询时会以查询条件整体去匹配词库中的词（分词后的单个词）
    QueryBuilder queryBuilder = QueryBuilders.termQuery("city", "北京");
    SearchResponse resp = esTemplate.search(queryBuilder, 60, "area");

    printResult(resp);
  }

  private void printResult(SearchResponse resp) {
    Arrays.stream(resp.getHits().getHits())
        .forEach(
            hit -> {
              log.info("{}｛｝{}", hit.getIndex(), hit.getSourceAsString(), hit.getType());
              log.info("{}", hit.toString());
              log.info(" {}", Jsons.json(hit.getSourceAsMap()));
            });
  }
}
