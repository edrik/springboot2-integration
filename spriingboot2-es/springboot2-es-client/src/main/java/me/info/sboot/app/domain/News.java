package me.info.sboot.app.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Amber
 * @version v0.0.1
 * @desc
 * @date 2020/12/21 15:41
 */
@Accessors(chain = true)
@Data
public class News implements Serializable {
  private Long id;
  private String title;
  private String tag;

  private String content;

  private LocalDateTime publishTime;
  private LocalDateTime created;
}
