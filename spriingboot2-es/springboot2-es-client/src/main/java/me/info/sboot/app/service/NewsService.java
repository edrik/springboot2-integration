package me.info.sboot.app.service;

import java.util.List;

import me.info.sboot.app.domain.News;

/**
 * @author Amber L
 * @version v0.0.1
 * @desc
 * @date 2020/12/25 16:50
 */
public interface NewsService {

  boolean createIndex();

  boolean saveAll(List<News> list);

  boolean delIndex(String indexName);
}
