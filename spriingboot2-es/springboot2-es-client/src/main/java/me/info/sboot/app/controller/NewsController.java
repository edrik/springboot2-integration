package me.info.sboot.app.controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import me.info.common.Randoms;
import me.info.sboot.app.domain.News;
import me.info.sboot.app.service.NewsService;

/**
 * @author Amber L
 * @version v0.0.1
 * @desc
 * @date 2020/12/25 16:50
 */
@Slf4j
@RestController
@RequestMapping("/news")
public class NewsController {
  @Autowired NewsService newsService;

  @GetMapping("")
  public void create() {
    newsService.createIndex();
  }

  @DeleteMapping("")
  public boolean delIndex(String indexName) {
    return newsService.delIndex(indexName);
  }

  @PostMapping("")
  public void addDoc() {

    List<News> list =
        Stream.iterate(1L, x -> x + 1)
            .limit(999)
            .map(
                x ->
                    new News()
                        .setId(x)
                        .setTitle(Randoms.title())
                        .setContent(Randoms.getChineseStr(56, 256))
                        .setPublishTime(LocalDateTime.now())
                        .setTag(Randoms.getChineseStr(2, 4)))
            .collect(Collectors.toList());
    newsService.saveAll(list);
  }
}
