package me.info.sboot.app.service;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import me.info.sboot.app.holder.EsTemplate;

/**
 * @author Amber
 * @version v0.0.1
 * @desc
 * @date 2020/12/21 10:08
 */
@Slf4j
@Service
public class DemoServiceImpl implements DemoService {

  @Autowired EsTemplate esTemplate;
  @Autowired RestHighLevelClient highLevelClient;


}
