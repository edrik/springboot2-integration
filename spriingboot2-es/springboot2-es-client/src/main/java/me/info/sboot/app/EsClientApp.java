package me.info.sboot.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Amber
 * @version v0.0.1
 * @desc
 * @date 2020/12/19 13:55
 */
@ComponentScan(basePackages = {"me.info.es", "me.info.sboot"})
@SpringBootApplication
public class EsClientApp {

  public static void main(String[] args) {
    SpringApplication.run(EsClientApp.class, args);
  }
}
