package me.info.sboot.app.cfg.prop;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

/**
 * @author Amber
 * @version v0.0.1
 * @desc
 * @date 2020/12/19 17:25
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "es")
public class EsProp {

  private String host;
  private Integer port = 9200;

  private String username;
  private String password;
  private Integer maxConnTotal;
  private Integer maxConnPreRoute;
  private Integer connectTimeout;
  private Integer socketTimeout;
  private Integer connectRequestTimeout;

  private Boolean uniqueConnectTimeout;
  private Boolean uniqueConnectNumConfig;
  //@Value("#{'$es.nodes'.split(',')}")
  private String[] nodes;

}
