package me.info.common.func;

import java.util.function.Supplier;

/**
 * @author Amber L
 * @version v0.0.1
 * @desc
 * @date 2020/12/26 11:35
 */
@FunctionalInterface
public interface ThrowableSupplier<T, E extends Exception> {
  Supplier<T> get() throws E;
}
