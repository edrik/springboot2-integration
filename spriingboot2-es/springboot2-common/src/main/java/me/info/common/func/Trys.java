package me.info.common.func;

import java.util.function.Supplier;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Amber L
 * @version v0.0.1
 * @desc
 * @date 2020/12/26 11:26
 */
@Slf4j
public class Trys {
  public static <T, E extends Exception> Supplier<T> of(ThrowableSupplier<T, E> supplier) {
    return () -> {
      try {
        return (T) supplier.get();
      } catch (Exception e) {
        log.error(e.getMessage(), e);
      }
      return null;
    };
  }

  public static <E extends Exception, T> Supplier<T> wrap(ThrowableSupplier<T,E> generateMapping) {
    return ()->{
      try {
        return (T) generateMapping.get();
      } catch (Exception e) {
        e.printStackTrace();
      }
      return null;
    };
  }
}
