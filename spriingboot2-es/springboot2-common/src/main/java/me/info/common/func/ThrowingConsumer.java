package me.info.common.func;

/**
 * @author Amber L
 * @version v0.0.1
 * @desc
 * @date 2020/12/26 12:21
 */
@FunctionalInterface
public interface ThrowingConsumer<T, E extends Exception> {
  void accept(T t) throws E;
}