package me.info.common;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;

/**
 * @author Amber L
 * @version v0.0.1
 * @desc
 * @date 2020/12/25 16:42
 */
public class Jsons {
  public static String json(Object obj) {
    return JSON.toJSONString(obj);
  }

  public static Map<String, Object> parseMap(String json) {
    return JSON.parseObject(json, HashMap.class);
  }
}
